# socket_implementation

## Getting started

### Server

To compile the server socket:

```bash
cd server
make
```

To run the server:

```bash
make run_server
```

### Client

You can copy the code to your dotnet project or init one over the folder `client`

## Project status

### Sources:

This code has some customizations, but it is based in the following articles:

* CPP Sockets: https://tldp.org/LDP/LG/issue74/tougher.html
* C# Socket: https://www.c-sharpcorner.com/article/socket-programming-in-C-Sharp/

## Help

If have ay problem connecting the socket in WSL with the socket in Windows,
follow this guide:

https://learn.microsoft.com/es-es/windows/wsl/networking
